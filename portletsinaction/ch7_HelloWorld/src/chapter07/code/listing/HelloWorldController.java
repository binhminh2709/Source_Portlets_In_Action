package chapter07.code.listing;

import java.util.HashMap;
import java.util.Map;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.springframework.web.portlet.ModelAndView;
import org.springframework.web.portlet.mvc.Controller;

/**
 * Spring controller which implements the Controller interface.
 * 
 * @author asarin
 *
 */
//-- (1)Request handler implements Controller interface
public class HelloWorldController implements Controller {
	/**
	 * action method.
	 */
	public void handleActionRequest(ActionRequest request, ActionResponse response) throws Exception {
		//-- do nothing the Hello World portlet doesn't receive action requests.
	}
	
	/**
	 * render method.
	 */
	public ModelAndView handleRenderRequest(RenderRequest request, RenderResponse response) throws Exception {
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("helloWorldMessage", "Hello World");
		return new ModelAndView("helloWorld", model);//-- (2)handleRenderRequest returns ModelAndView
	}
	
	/**
	 * The HelloWorldController portlet  request  handler  implements  
	 * the org.springframework.web.portlet.mvc.Controllerinterface (1), 
	 * which defines two methods:
	 * ■ handleActionRequest—This is the same as portlet’s action method 
	 * ■ handleRenderRequest—This is the same as portlet’s render method
	 * The HelloWorldController’s handleRenderRequestmethod returns a ModelAndView
	 * object (2) that  contains  the model data  and view information.
	 * You’ve  seen  in  earlier chapters that a render method creates content by dispatching a render request to a JSPpage,
	 * and the data that needs to be rendered by the JSPpage is passed as request attributes.
	 * In Spring Portlet MVC, the ModelAndViewobject holds the model data (the data to be rendered)
	 * and the view information (the JSPpage), and it’s used by the Spring Portlet MVCframework to dispatch 
	 * the render request to the view and to pass the model data. 
	 * */
}
